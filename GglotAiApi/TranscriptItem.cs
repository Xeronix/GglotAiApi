namespace GglotAiApi;

/// <summary>
/// Represents a transcript item containing text and its corresponding International Phonetic Alphabet (IPA) transcription.
/// </summary>
public class TranscriptItem
{
    /// <summary>
    /// Gets or sets the original text for the transcript.
    /// </summary>
    public string Text { get; set; }

    /// <summary>
    /// Gets or sets the list of International Phonetic Alphabet (IPA) transcriptions for the text.
    /// </summary>
    public List<string> Ipa { get; set; }
}
