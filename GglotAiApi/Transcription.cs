using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace GglotAiApi
{
    /// <summary>
    /// This class has a method that allows you to transcribe the text
    /// </summary>
    public class Transcription
    {
        private readonly HttpClient _httpClient;
        private readonly HttpClientHandler _httpClientHandler = new HttpClientHandler();

        /// <summary>
        /// Constructor for initial initialization of necessary classes and variables
        /// </summary>
        public Transcription()
        {
            _httpClientHandler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            _httpClient = new HttpClient(_httpClientHandler);
            _httpClient.BaseAddress = new Uri("https://dics.glot.ai/");
            _httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        /// <summary>
        /// Retrieves the phonetic transcriptions of words or a single word using an external service.
        /// </summary>
        /// <param name="paragraph">A list of words or a single word for which to obtain phonetic transcriptions.</param>
        /// <returns>A list of TranscriptItem objects representing the phonetic transcriptions of the input words.</returns>
        /// <remarks>
        /// This method sends a request to an external service to obtain the phonetic transcriptions of the provided words.
        /// The input parameter 'paragraph' can contain a single word or a list of words. Each word will be transcribed into its
        /// corresponding phonetic transcription using the International Phonetic Alphabet (IPA).
        /// The method returns a list of TranscriptItem objects, each containing the original word and its associated list of IPA transcriptions.
        /// If a word has multiple possible transcriptions, all variations will be included in the returned list.
        /// </remarks>
        public async Task<List<TranscriptItem>> GetTranscript(string paragraph)
        {
            var requestBody = new { paragraph };
            var jsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(requestBody);
            var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _httpClient.PostAsync("vocab/transcript", httpContent);
            string jsonResponse = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<TranscriptItem>>(jsonResponse);
        }
    }
}