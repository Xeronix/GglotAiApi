# GglotAiApi
<img src="https://gglot.com/wp-content/uploads/2023/03/Wordmark-Colored-Light-mode-2.png" width="20%">

### Unofficial API library for transcribing text into phonetic transcription
<h2>  
   <a href="https://www.nuget.org/packages/GglotAiApi/">  
      <img src="https://img.shields.io/nuget/dt/GglotAiApi?style=flat">  
   </a>
   <a href="https://gitlab.com/Xeronix/GglotAiApi/-/blob/main/GglotAiApi.csproj">  
      <img src="https://img.shields.io/badge/.NET-Standard%202.0-blueviolet?style=flat">  
   </a>
   <img src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103?style=flat" >
   <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat">
   <a href="https://mastodon.social/@kirmozor">  
      <img src="https://img.shields.io/mastodon/follow/108763242400809137">  
   </a>
   <img src="https://img.shields.io/gitlab/stars/Xeronix/YandexMusicApi.svg?style=flat">
</h2>

## Description
I wrote this small library for my language learning program [VocabEase](https://gitlab.com/Xeronix/VocabEase). It is necessary for transcribing words into phonetic transcription. It uses the API of the [GglotAi](https://gglot.com) service.
## Installing
1. Install the .NET Core SDK on your computer if it is not already installed. You can download it from the official website: https://dotnet.microsoft.com/download

2. Open the terminal or command prompt on your computer.

3. Enter the following command in the terminal or command prompt:

    * dotnet add package GglotAiApi

4. Wait for the installation to complete. After that, you can use library in your C# project.

## Usage
To tell the truth, there is nothing else there except Transcription class, and that has 1 method, but the example of using it is below:

```
using GglotAiApi;

class Program
{
    static async Task Main()
    {
        Transcription transcription = new Transcription();
        List<TranscriptItem> transcripts = await transcription.GetTranscript("Your paragraph here");

        foreach (var transcript in transcripts)
        {
            Console.WriteLine($"Text: {transcript.Text}");
            Console.WriteLine("IPA:");
            foreach (var ipa in transcript.Ipa)
            {
                Console.WriteLine(ipa);
            }
            Console.WriteLine();
        }
    }
}
```
